/*============================================================
Nama File       : LCD.java 
Author          : Ila Virnanda Fitriana (225150701111011)
                  (Teknologi Informasi-B)
Last Update     : 15-Maret-2023
Update v1.0     : 15-Maret-2023  
============================================================*/

public class LCD {
    private String Status;
    private int Volume;
    private int Brightness;
    private String Cable;

    public String turnOff(){
        return Status;
    }
    public String turnOn(){
        return Status;
    }
    public String Freeze(){
        return Status;
    }
    public int volumeUp(){
        return Volume;
    }
    public int volumeDown(){
        return Volume;
    }
    public int brightnessUp(){
        return Brightness;
    }
    public int brightnessDown(){
        return Brightness;
    }
    public String cableUp(){
        return Cable;
    }
    public String cableDown(){
        return Cable;
    }
    public void setCable(String s ){
        Cable=s;
    }
    public void setVolume(int i) {
        Volume=i;
    }
    public void setStatus(String s ){
        Status=s;
    }
	public void setBrightness(int i) {
        Brightness=i;
	}
    public void displayMessage(){
        System.out.println("---------LCD---------");
        System.out.println("Status LCD saat ini       : "+Status);
        System.out.println("Volume LCD saat ini       : "+Volume);
        System.out.println("Brightness LCD saat ini   : "+Brightness);
        System.out.println("Cable LCD saat ini        : "+Cable);
    }
}
