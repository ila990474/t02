/*============================================================
Nama File       : main.java
Tugas           : menampilkan Status, Volume, Brightness, Cable
Versi           : 1.0
Fungsi          : mendefiniskan kelas 
Author          : Ila Virnanda Fitriana (225150701111011)
                  (Teknologi Informasi-B)
Last Update     : 15-Maret-2023
Update v1.0     : 15-Maret-2023
============================================================*/

public class main {

public static void main (String []args){
        LCD lcd1 = new LCD();
        lcd1.turnOff();
        lcd1.turnOn();
        lcd1.cableDown();
        lcd1.cableUp();
        lcd1.Freeze();
        lcd1.volumeUp();

        lcd1.setVolume(25);
        lcd1.setBrightness(20);
        lcd1.setCable("HDMI");
        lcd1.setStatus("Freezer");


        lcd1.displayMessage();
}
}
